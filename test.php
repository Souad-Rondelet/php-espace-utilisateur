<?php
$bdd = new PDO('mysql:host=localhost;dbname=formulaire;charset=utf8', 'root', '');

if (
    isset($_POST['nom']) &&
    isset($_POST['prenom']) &&
    isset($_POST['Phone']) &&
    isset($_POST['email']) &&
    isset($_POST['confirmemail']) &&
    isset($_POST['password']) &&
    isset($_POST['confirmPass']) &&
    isset($_POST['subject']) &&
    isset($_POST['textarea']) &&
    isset($_POST['submit'])
) {
    if (
        !empty($_POST['nom']) &&
        !empty($_POST['prenom']) &&
        !empty($_POST['Phone']) &&
        !empty($_POST['email']) &&
        !empty($_POST['confirmemail']) &&
        !empty($_POST['password']) &&
        !empty($_POST['confirmPass']) &&
        !empty($_POST['subject']) &&
        !empty($_POST['textarea'])
    ) {


        $Nom = htmlspecialchars($_POST['nom']);
        $Prenom = htmlspecialchars($_POST['prenom']);
        $Phon = htmlspecialchars($_POST['Phone']);
        $Email = htmlspecialchars($_POST['email']);
        $Confimemal = htmlspecialchars($_POST['confirmemail']);
        $Password = hash('sha256', ($_POST['password']));
        $Confirpass = hash('sha256', ($_POST['confirmPass']));
        $Subject = htmlspecialchars($_POST['subject']);
        $Textarea = htmlspecialchars($_POST['textarea']);

        if (filter_var($Email, FILTER_VALIDATE_EMAIL)) {

            if ($Email == $Confimemal) {


                $query = $bdd->prepare('SELECT * FROM données WHERE email = :email');
                $query->execute([
                    ':email' => $Email,
                ]);

           

                if ($Password == $Confirpass) {

                    $requet = $bdd->prepare('INSERT INTO données (nom, prenom, telephon, email, password, sujet, message)
                        VALUES (:nom, :prenom, :Phone, :email, :password, :subject, :textarea)');
                    $requet->execute([
                        ':nom' => $Nom,
                        ':prenom' => $Prenom,
                        ':Phone' => $Phon,
                        ':email' => $Email,
                        ':password' => $Password,
                        ':subject' => $Subject,
                        ':textarea' => $Textarea,
                    ]);
                } else {
                    echo "pasword is not valid";
                }
            } else {
                echo "$Email not confirm";
            }
        } else {
            echo "email not valide";
        }
    } else {
        echo "All fields must be completed";
    }
}




?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link href="css/test.css" rel="stylesheet">
</head>

<body>

    <form action="connect-test.php" method="POST" class="fom">

        <h1>Sign Up</h1>


        <label class="llam">First Name:</br>
        </label>
        <input type="text" name="nom" class="box" placeholder="Your first name" /></br>




        <label class="llam">Last Name:</br>
        </label>
        <input type="text" name="prenom" class="box" placeholder="Your last name" /></br>




        <label class="llam">Phone number:</br>
        </label>
        <input type="tel" name="Phone" class="box" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" placeholder="123-456-7894" required /></br>



        <label class="llam">Email:</br>
        </label>
        <input type="email" name="email" class="box" placeholder="Your last name" /></br>



        <label class="llam" for="confirmeEmail">Cinfirm email:</br>
        </label>
        <input type="text" name="confirmemail" class="box" placeholder="Your last name" /></br>



        <label class="llam" for="password">Password:</br>
        </label>
        <input type="password" name="password" class="box" /></br>



        <label class="llam" for="confirmpass">confirm Password:</br>
        </label>
        <input type="password" name="confirmPass" class="box" /></br>




        <label class="llam" for="subject">Subject:</br>
        </label>
        <input type="text" name="subject" class="box" placeholder="why you writing to us" /></br>


        <label class="llam" for="name">Message:</br>
        </label>
        <textarea class="lol" class="box1" name="textarea">
        </textarea></br>


        <input type="submit" name="submit" class="so" value="Send Message">

    </form>

</body>

</html>