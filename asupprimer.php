<?php
$bdd = new PDO('mysql:host=localhost;dbname=exemple;charset=utf8', 'root', '');

if (
    isset($_POST['pseudo']) &&
    isset($_POST['email']) &&
    isset($_POST['confirmemail']) &&
    isset($_POST['password']) &&
    isset($_POST['confirmpassword']) &&
    isset($_POST['button'])
) {


    if (
        !empty($_POST['pseudo']) &&
        !empty($_POST['email']) &&
        !empty($_POST['confirmemail']) &&
        !empty($_POST['password']) &&
        !empty($_POST['confirmpassword'])
    ) {

        $Pseudo = htmlspecialchars($_POST['pseudo']);
        $Email = htmlspecialchars($_POST['email']);
        $Confirmemail = htmlspecialchars($_POST['confirmemail']);
       //$Password = password_hash(($_POST['password'], PASSWORD_DEFAULT);
        $Password = hash('sha256', ($_POST['password']));
        $ConfirmPassw = hash('sha256', ($_POST['confirmpassword']));
      

        $Pseudolength = strlen($Pseudo);
        if ($Pseudolength <= 100) {


            if(!preg_match("/^[a-zA-Z0-9]*$/", $Pseudo)){
            
                
                
                if (filter_var($Email, FILTER_VALIDATE_EMAIL)) {
                    
                    if ($Email == $Confirmemail) {
                        
                        
                        $query = $bdd->prepare('SELECT email FROM user WHERE email = :email');
                        $query->execute([
                            ':email' => $Email,
                            ]);
                            $result = $query->fetch();
                            if ($result == 0) {
                                
                                
                                
                                if (mb_strlen($Password) >= 9) {
                                    
                                    if ($Password == $ConfirmPassw) {
                                        
                                        $quey = $bdd->prepare('INSERT INTO user (pseudo, email, password, sign_date)
                     VALUES(:pseudo, :email, :password, CURRENT_DATE())');
                                $quey->execute([
                                    ':pseudo' => $Pseudo,
                                    ':email' => $Email,
                                    ':password' => $Password,
                                 
                                    ]);
                                    
                                      header("location:connecter-supp.php");
                                } else {
                                    $error = "Password is not confirm";
                                }
                            } else {
                                $error = "Password very shorts maximum 9 characters";
                            }
                        } else {
                            $error = "Mail adress si already existe";
                        }
                    } else {
                        $error = " .$Email. votre mail n'est pas confirme";
                    }
                } else {
                    $error = "votre mail n'est pas valide";
                }
            }
            else {
                $error="Votre .$Pseudo doit avoir des character majuscule, minuscule et chiffres";
            }
            } else {
                $error = "votre pseudo ne doit pas dépasser 100 characters";
        }
    } else {
        $error = "All filed must be completed";
    }
}




?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>

    <form action="" method="POST" style="text-align: center">
        <label for="">Pseudo:</br>
            <input type="text" name="pseudo" placeholder="inser your pseudo" style="width: 200px; height:30px; border-radius: 3px;" value="<?php if (isset($Pseudo)) {
                                                                                                                                                echo $Pseudo;
                                                                                                                                            } ?>" /></br>
        </label>

        <label for="Email">Email:</br>
            <input type="email" name="email" placeholder="insert your email" style="width: 200px; height:30px; border-radius: 3px;"
             value="<?php if (isset($Email)) {
                                                                                                                                                echo $Email;
                                                                                                                                            } ?>" /></br>
        </label>

        <label for="confirmemail">Confirme your email:</br>
            <input type="email" name="confirmemail" placeholder="confirme your email" style="width: 200px; height:30px; border-radius: 3px;" value="<?php if (isset($Confirmemail)) {
                                                                                                                                                        echo $Confirmemail;
                                                                                                                                                    } ?>" /></br>
        </label>


        <label for="password">Password:</br>
            <input type="password" name="password" style="width: 200px; height:30px; border-radius: 3px;" /></br>
        </label>

        <label for="confirmpassword">Confirme your password:</br>
            <input type="password" name="confirmpassword" style="width: 200px; height:30px; border-radius: 3px;" /></br>
        </label>

        <input type="submit" value="Sign Up" name="button" />



    </form>


    <?php
    if (isset($error)) {
        echo "<div style='text-align:center; color:red;'>" . $error . "</div>";
    }
    ?>

</body>

</html>